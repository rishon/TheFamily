CREATE DATABASE `family` DEFAULT CHARACTER SET utf8;

USE `family`;

CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT '' COMMENT '姓名',
  `formerName` varchar(32) DEFAULT '' COMMENT '曾用名/别名',
  `nickName` varchar(32) DEFAULT '' COMMENT '小名/绰号',
  `sex` enum('M','F') DEFAULT 'M' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '公历生日',
  `bNong` varchar(32) DEFAULT '' COMMENT '农历生日',
  `bNongStr` varchar(32) DEFAULT '' COMMENT '农历生日字符串',
  `deathday` date DEFAULT NULL COMMENT '公历忌日',
  `dNong` varchar(32) DEFAULT '' COMMENT '农历忌日',
  `dNongStr` varchar(32) DEFAULT '' COMMENT '农历忌日字符串',
  `homeAddress` varchar(64) DEFAULT '' COMMENT '家庭住址',
  `homeTell` varchar(16) DEFAULT '' COMMENT '住宅电话',
  `workUnits` varchar(64) DEFAULT '' COMMENT '工作单位',
  `workAddress` varchar(64) DEFAULT '' COMMENT '工作地址',
  `phoneNumber` varchar(16) DEFAULT '' COMMENT '手机',
  `babaId` varchar(16) DEFAULT '' COMMENT '爸爸',
  `mamaId` varchar(16) DEFAULT '' COMMENT '妈妈',
  `spouseId` varchar(16) DEFAULT '' COMMENT '配偶',
  `tbId` varchar(16) DEFAULT '' COMMENT '兄弟姐妹ID',
  `znId` varchar(16) DEFAULT '' COMMENT '子女ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `personId` INT(11) DEFAULT NULL,
  `userName` VARCHAR(20) DEFAULT '' COMMENT '用户名',
  `nickName` VARCHAR(32) DEFAULT '' COMMENT '呢称',
  `password` VARCHAR(64) DEFAULT '' COMMENT '密码',
  `userType` ENUM('M','U') DEFAULT 'U' COMMENT '用户类型',
  `createTime` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
