package com.liuzy.family.mapper;

import java.util.List;
import java.util.Map;

import com.liuzy.family.entity.CalendarEntity;

public interface CalendarEntityMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(CalendarEntity record);

	CalendarEntity selectByPrimaryKey(Integer id);

	List<CalendarEntity> selectAll();

	int updateByPrimaryKey(CalendarEntity record);

	CalendarEntity getByYang(String date);// 1901-1-1

	CalendarEntity getByNong(String date);// 1901-11-11

	CalendarEntity nextNongDay(Map<String, Object> params);// 八月十二

	CalendarEntity today();
}