package com.liuzy.family.config;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class ProjectConfigurer extends PropertyPlaceholderConfigurer {

	private static Properties props = null;

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
		ProjectConfigurer.props = props;
		super.processProperties(beanFactoryToProcess, props);
	}

	public static String get(String key) {
		return props.getProperty(key);
	}
}
